package importer

import (
	"fmt"
	api "gitlab.com/walnuss0815/mayan-edms-client/client"
	"gitlab.com/walnuss0815/mayan-edms-client/models"
	"gitlab.com/walnuss0815/mayan-importer/fs"
	"strings"
)

type Importer struct {
	api api.Client
}

func NewImporter(url string, username string, password string, ignoreTlsError bool) *Importer {
	i := new(Importer)
	i.api = *api.NewClient(url, username, password, ignoreTlsError)
	return i
}

func (c Importer) CreateDocument(file *fs.File, t *models.DocumentType) (*models.NewDocument, error) {
	isFile, err := fs.IsFile(file.Path)
	if err != nil {
		return nil, err
	}

	if !isFile {
		return nil, fmt.Errorf("%s is not a fs", file.Path)
	}

	document, err := c.api.CreateDocument(file.Path, t.Id)
	if err != nil {
		return nil, err
	}

	return document, nil
}

func (c Importer) GetDocumentTypeByLabel(label string) (*models.DocumentType, error) {
	types, err := c.api.GetDocumentTypes()
	if err != nil {
		return nil, err
	}

	for _, t := range types {
		if strings.ToLower(t.Label) == strings.ToLower(label) {
			return &t, nil
		}
	}

	return nil, fmt.Errorf("there is no document type for label %s", label)
}
