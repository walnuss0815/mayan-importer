# Mayan Importer

The Mayan Importer helps to import local documents to a remote [Mayan EDMS](https://www.mayan-edms.com/) server.

## Configuration

The Mayan Importer can be configured by a YAML file or environment variables. Ab example YAML configuration file can be found in the [repository](/-/blob/master/config.yaml.template).