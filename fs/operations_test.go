package fs

import (
	"fmt"
	"testing"
)

func TestFilterExtension(t *testing.T) {
	files := []File{*NewFile("test.pdf"), *NewFile("test.xlsx"), *NewFile("test.jpg")}

	files = FilterExtension(files, ".pdf")

	fmt.Printf("%+v", files)

	expectedFiles := []File{*NewFile("test.pdf")}

	if !equal(files, expectedFiles) {
		t.Fatalf("files does not equal expected files: %+v != %+v", files, expectedFiles)
	}
}

func equal(a, b []File) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}
