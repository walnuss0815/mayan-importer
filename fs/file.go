package fs

import (
	"os"
	"path/filepath"
)

type File struct {
	Path string
}

func NewFile(path string) *File {
	f := new(File)
	f.Path = path
	return f
}

func GetFiles(root string) ([]File, error) {
	files := make([]File, 0)

	err := filepath.Walk(root,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}

			if !info.IsDir() {
				file := NewFile(path)
				files = append(files, *file)
			}

			return nil
		})
	if err != nil {
		return nil, err
	}

	return files, nil
}

func (f File) Clean() error {
	err := os.Remove(f.Path)
	if err != nil {
		return err
	}

	directory := f.Path
	isEmpty := true
	for isEmpty {
		directory = filepath.Dir(directory)
		isEmpty, err = CheckIfDirectoryEmpty(directory)
		if err != nil {
			return err
		}

		if isEmpty {
			err := os.Remove(directory)
			if err != nil {
				return err
			}
		}
	}

	return nil
}
