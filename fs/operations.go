package fs

import (
	"io"
	"os"
	"strings"
)

func FilterExtension(files []File, extensions []string) []File {
	newFiles := make([]File, 0)
	for _, file := range files {
		retain := false
		for _, ext := range extensions {
			if strings.HasSuffix(strings.ToLower(file.Path), strings.ToLower(ext)) {
				retain = true
			}
		}

		if retain {
			newFiles = append(newFiles, file)
		}
	}
	return newFiles
}

func CheckIfDirectoryEmpty(path string) (bool, error) {
	f, err := os.Open(path)
	if err != nil {
		return false, err
	}
	defer f.Close()

	_, err = f.Readdirnames(1)
	if err == io.EOF {
		return true, nil
	}
	return false, err
}

func IsFile(path string) (bool, error) {
	fi, err := os.Stat(path)
	if err != nil {
		return false, err
	}

	mode := fi.Mode()

	return mode.IsRegular(), nil
}
