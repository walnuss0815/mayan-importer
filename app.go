package main

import (
	"github.com/ilyakaznacheev/cleanenv"
	"gitlab.com/walnuss0815/mayan-importer/config"
	"gitlab.com/walnuss0815/mayan-importer/fs"
	"gitlab.com/walnuss0815/mayan-importer/importer"
	"log"
	"os"
)

func main() {
	var cfg config.Config
	err := cleanenv.ReadEnv(&cfg)
	if err != nil {
		log.Panicf("Error loading config: %s", err.Error())
	}

	configFile := "config.yaml"
	_ = cleanenv.ReadConfig(configFile, &cfg)

	imp := importer.NewImporter(cfg.Mayan.Url, cfg.Mayan.Username, cfg.Mayan.Password, cfg.Mayan.IgnoreTlsError)

	log.Print("Collecting documents...")
	files, err := fs.GetFiles(cfg.Importer.Directory)
	if err != nil {
		log.Fatalf("Unable to collect documents: %s", err.Error())
	}

	files = fs.FilterExtension(files, cfg.Importer.FileExtensions)

	log.Printf("Found %v documents", len(files))

	if len(files) == 0 {
		os.Exit(1)
	}

	documentType, err := imp.GetDocumentTypeByLabel(cfg.Importer.DocumentType)
	if err != nil {
		log.Fatalf("Error getting document type: %s", err.Error())
	}

	for _, f := range files {
		document, err := imp.CreateDocument(&f, documentType)
		if err != nil {
			log.Fatalf("Unable to create document: %s", err.Error())
		}

		err = f.Clean()
		if err != nil {
			log.Fatalf("Unable to remove fs: %s", err.Error())
		}

		log.Printf("Document %s (%d) has been added", document.Label, document.Id)
	}
}
