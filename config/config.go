package config

type Config struct {
	Mayan struct {
		Url            string `yaml:"url" env:"MAYAN_IMPORTER_URL" env-default:"http://localhost"`
		Username       string `yaml:"username" env:"MAYAN_IMPORTER_USERNAME" env-default:"admin"`
		Password       string `yaml:"password" env:"MAYAN_IMPORTER_PASSWORD" env-default:"passw0rd"`
		IgnoreTlsError bool   `yaml:"ignore_tly_error" env:"MAYAN_IMPORTER_IGNORE_TLS_ERROR" env-default:"false"`
	} `yaml:"mayan"`
	Importer struct {
		DocumentType   string   `yaml:"type" env:"MAYAN_IMPORTER_TYPE" env-default:"default"`
		Directory      string   `yaml:"directory" env:"MAYAN_IMPORTER_DIRECTORY" env-default:"."`
		FileExtensions []string `yaml:"extensions" env:"MAYAN_IMPORTER_EXTENSIONS" env-separator:"," env-default:".pdf,.jpg"`
	} `yaml:"importer"`
}
